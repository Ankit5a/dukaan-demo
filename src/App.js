import React from 'react';
import Header from './Components/Header';
import SloganMaker from './Components/SloganMaker';
import Features from './Components/Features';
import OtherProducts from './Components/OtherProducts';
import Footer from './Components/Footer';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <SloganMaker />
      <Features />
      <OtherProducts />
      <Footer />
    </div>
  );
}

export default App;
