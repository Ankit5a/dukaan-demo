import React from 'react';
import madeIn from '../Assets/made-in.png';
import logo from '../Assets/logo.png';
import '../Styles/Footer.css';

function Footer() {
    return (
        <div id="footer">
            <section className="container">
                <div className="footer-links">
                    <div className="footer-logo-wrap">
                        <img className="logo" src={logo} alt="company logo" />
                    </div>
                    <div className="footer-links-wrap">
                        <ul>
                            <li><a href="#contact-us">Contact</a></li>
                            <li><a href="#faqs">FAQ's</a></li>
                        </ul>
                        <ul>
                            <li><a href="#tutorials">Tutorials</a></li>
                            <li><a href="#blog">Blog</a></li>
                        </ul>
                        <ul>
                            <li><a href="#privacy">Privacy</a></li>
                            <li><a href="#banned-items">Banned Items</a></li>
                        </ul>
                        <ul>
                            <li><a href="#about">About</a></li>
                            <li><a href="#jobs">Jobs<span className="badge">3</span></a></li>
                        </ul>
                        <ul>
                            <li><a href="#facebook">Facebook</a></li>
                            <li><a href="#twitter">Twitter</a></li>
                            <li><a href="#linked-in">Linked In</a></li>
                        </ul>
                    </div>
                </div>
                <div className="company-details-section">
                    <div className="company-rights">Dukaan 2020, All rights reserved.</div>
                    <img className="made-in-logo" src={madeIn} alt="made in india" />
                </div>
            </section>
        </div>
    );
}

export default Footer;
