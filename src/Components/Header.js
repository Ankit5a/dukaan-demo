import React from 'react';
import logo from '../Assets/logo.png';
import '../Styles/Header.css';

function Header() {
    return (
        <div id="header">
            <div className="header-container">
                <img className="logo" src={logo} alt="main logo" />
                <div className="header-menu-items">
                    <a href="#sign-in" id="sign-in">Sign In</a>
                    <button id="menu-button">
                        Dukaan for PC
                    </button>
                </div>
            </div>
        </div>
    );
}

export default Header;
