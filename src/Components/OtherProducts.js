import React from 'react';
import Product1 from '../Assets/product-1.png';
import Product2 from '../Assets/product-2.png';
import Product3 from '../Assets/product-3.png';
import Product4 from '../Assets/product-4.png';
import '../Styles/OtherProducts.css';

function OtherProducts() {
    return (
        <div id="other-products">
            <div className="other-products-container">
                <h2 id="other-products-header">Try our other free products</h2>
                <div className="products-list-wrap">
                    <div className="card">
                        <img src={Product1} alt="product 1" />
                        <div className="card-body">
                            <p className="card-header">Privacy Policy Generator</p>
                            <span className="card-subheader">
                                Stock your store with 100s of products and start selling to customers in minutes, without the hassle of inventory or packaging.
                            </span>
                        </div>
                    </div>
                    <div className="card">
                        <img src={Product2} alt="product 2" />
                        <div className="card-body">
                            <p className="card-header">Terms &#38; Conditions Generator</p>
                            <span className="card-subheader">
                                Stock your store with 100s of products and start selling to customers in minutes, without the hassle of inventory or packaging.
                            </span>
                        </div>
                    </div>
                    <div className="card">
                        <img src={Product3} alt="product 3" />
                        <div className="card-body">
                            <p className="card-header">Domain Name Generator</p>
                            <span className="card-subheader">
                                Stock your store with 100s of products and start selling to customers in minutes, without the hassle of inventory or packaging.
                            </span>
                        </div>
                    </div>
                    <div className="card">
                        <img src={Product4} alt="product 4" />
                        <div className="card-body">
                            <p className="card-header">Invoice Generator</p>
                            <span className="card-subheader">
                                Stock your store with 100s of products and start selling to customers in minutes, without the hassle of inventory or packaging.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default OtherProducts;
