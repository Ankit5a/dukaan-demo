import React, { useState, useEffect, useRef } from 'react';
import randomParagraph from 'random-paragraph';
import tippy from 'tippy.js';
import Loading from '../Assets/loading.gif';
import 'tippy.js/dist/tippy.css';
import '../Styles/SloganMaker.css';


function SloganMaker() {
  const [value, setValue] = useState('cozy');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState([]);
  const tooltipInstance = useRef(null);

  useEffect(() => {
    if (results.length) {
      tooltipInstance.current = tippy('.sentence', {
        placement: 'left',
        content: 'Click to copy',
        interactive: true,
        arrow: false,
      });
    }
  });
  
  useEffect(() => {
    const sentences = randomParagraph({ sentences: 10 });
    setResults(sentences.split('. '));
  }, []);

  const handleChange = (e) => {
    setValue(e.target.value);
    if (e.target.value.trim() !== '') {
      setError(null);
    }
  };

  const handleSubmit = async () => {
    if (value.trim() === '') {
      setError('Please enter a valid word');
    } else {
      setLoading(true);
      setTimeout(() => {
        const sentences = randomParagraph({ sentences: 10 });
        setResults(sentences.split('. '));
        setLoading(false);
      }, 1000);
    }
  };

  const handlePageClick = () => {
    setLoading(true);
    setTimeout(() => {
      const sentences = randomParagraph({ sentences: 10 });
      setResults(sentences.split('. '));
      setLoading(false);
    }, 1000);
  };

  const handleClick = (value, i) => {
    if (tooltipInstance.current !== null) {
      tooltipInstance.current[i].setContent('Copied');
      tooltipInstance.current[i].show();
    }
  };

  return (
      <div id="slogan-maker">
        <div className="slogan-maker-container">
          <h2 id="slogan-maker-header">Free slogan maker</h2>
          <p id="slogan-maker-description">Simply enter a term that describes your business, and get up to 1,000 relevant slogans for free.</p>
          <div className="form-container">
            <span className="input-label">Word for your slogan</span>
            <input id="search" type="search" onChange={handleChange} value={value} />
            {error ? <span className="error">{error}</span> : null}
            <button disabled={loading} id="submit-btn" onClick={handleSubmit}>Generate slogans</button>
          </div>
          <div id="results">
            {loading ? (
              <div id="loading">
                <img src={Loading} alt="loading" />
              </div>
            ) : (
              <>
                <div className="sentences-wrap">
                  {results.map((sentence, i) => (
                    <span onClick={() => handleClick(sentence, i)} key={sentence} className="sentence">
                      {sentence}
                    </span>
                  ))}
                </div>
                <div className="pagination-wrap">
                  <nav className="pagination">
                    <div />
                    <div className="pages">
                      <span className="page">
                        <span className="paging active" onClick={handlePageClick}>1</span>
                      </span>
                      <span className="page">
                        <span className="paging" onClick={handlePageClick}>2</span>
                      </span>
                      <span className="page">
                        <span className="paging" onClick={handlePageClick}>3</span>
                      </span>
                      <span className="dots">…</span>
                      <span className="paging" onClick={handlePageClick}>21</span>
                    </div>
                    <span className="next-page" onClick={handlePageClick}>{`Next >`}</span>
                  </nav>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
  );
}

export default SloganMaker;
